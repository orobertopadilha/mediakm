package br.edu.unisep.mediakm

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import br.edu.unisep.mediakm.databinding.ActivityMainBinding
import com.google.android.material.textfield.TextInputEditText
import java.text.NumberFormat

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupEvents()
    }

    private fun calculateAverage() {
        val currentKm = binding.etCurrentKm.text.toString().toFloat()
        val previousKm = binding.etPreviousKm.text.toString().toFloat()
        val amount = binding.etAmount.text.toString().toFloat()

        val result = (currentKm - previousKm) / (50 - amount)
        val formatter = NumberFormat.getNumberInstance().apply {
            maximumFractionDigits = 1
            minimumFractionDigits = 1
        }

        binding.tvResult.text = formatter.format(result)
        binding.tvResult.visibility = View.VISIBLE
        binding.tvKmL.visibility = View.VISIBLE
    }

    private fun clear() {
        binding.etCurrentKm.setText("")
        binding.etPreviousKm.setText("")
        binding.etAmount.setText("")

        binding.etPreviousKm.requestFocus()

        binding.tvResult.visibility = View.GONE
        binding.tvKmL.visibility = View.GONE
    }

    private fun setupEvents() {
        binding.btnCalculate.setOnClickListener { calculateAverage() }
        binding.btnClear.setOnClickListener { clear() }
    }
}